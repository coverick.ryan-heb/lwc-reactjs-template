import React from 'react';
import ReactDOM from 'react-dom';
import ReactTemplateApp from './App';

ReactDOM.render(<ReactTemplateApp />, document.getElementById('root'));
